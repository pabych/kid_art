<?php

namespace App\FrontendBundle\Controller;

use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\ArticleCategory;
use App\CoreBundle\Entity\ProductOrder;
use App\FrontendBundle\Form\ProductOrderType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Proxies\__CG__\App\CoreBundle\Entity\ProductCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="frontend_index")
     * @Template()
     */
    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        $articles = $em->getRepository('AppCoreBundle:Article')->findBy(array(), null, 3);
        $products = $em->getRepository('AppCoreBundle:Product')->findBy(array('isRecommended' => true));
        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'articles' => $articles,
            'products' => $products,
            'slides'=> $slides,
        );
    }

    /**
     * @Route("/client", name="frontend_client")
     * @Template()
     */
    public function clientAction() {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ArticleCategory $articleCategory */
        $articles = $articleCategory->getArticles();
        $categories = $em->getRepository('AppCoreBundle:ProductCategory')->findAll();

        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'articles' => $articles,
            'slides'=> $slides,
            'categories' => $categories
        );
    }

    /**
     * @Route("/article/{id}", name="frontend_article")
     * @Template()
     */
    public function articleAction($id) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Article $selected */
        $selected = $em->getRepository('AppCoreBundle:Article')->find($id);
        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();

        if(!$selected) {
            throw new Exception('Article does not exist');
        }

        $articles = [];
        /** @var ArticleCategory $category */
        foreach($selected->getCategories() as $category) {
            foreach($category->getArticles() as $article) {
                $articles[$article->getId()] = $article;
            }
        }

        $articleCategory = $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'selected' => $selected,
            'articles' => $articles,
            'slides' => $slides,
        );
    }

    /**
     * @Route("/catalog/{id}", name="frontend_catalog")
     * @Template()
     */
    public function catalogAction($id = null) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var ArrayCollection $categories */
        $categories = $em->getRepository('AppCoreBundle:ProductCategory')->findAll();

        if($id == null) {
            $products = $em->getRepository('AppCoreBundle:Product')->findAll();
        } else {
            /** @var ProductCategory $category */
            $category = $em->getRepository('AppCoreBundle:ProductCategory')->find($id);
            $products = $category->getProducts()->getValues();
        }

        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();

        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        $productsInBasket = $this->getProducts();

        $count = 0;
        foreach($productsInBasket as $item) {
            $count+=$item;
        }

        return array(
            'articleCategory'=> $articleCategory,
            'categories' => $categories,
            'products' => $products,
            'slides' => $slides,
            'count' => $count
        );
    }

    /**
     * @Route("/product/{id}", name="frontend_product")
     * @Template()
     */
    public function productAction($id) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository('AppCoreBundle:Product')->find($id);
        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $categories = $em->getRepository('AppCoreBundle:ProductCategory')->findAll();

        if(!$product) {
            throw new Exception('Product does not exist');
        }

        $productsInBasket = $this->getProducts();

        $count = 0;
        foreach($productsInBasket as $item) {
            $count+=$item;
        }

        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'product' => $product,
            'slides' => $slides,
            'categories' => $categories,
            'count' => $count
        );
    }

    /**
     * @Route("/contact", name="frontend_contact")
     * @Template()
     */
    public function contactAction() {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();


        $info = $this->container->getParameter('contact');

        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'slides' => $slides,
            'info' => $info,
        );
    }

    /**
     * @Route("/promo", name="frontend_promo")
     * @Template()
     */
    public function promoAction() {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'slides' => $slides,
        );
    }

    /**
     * @Route("/cookie", name="frontend_cookie")
     * @Template()
     */
    public function cookieAction(Request $request) {
        $cookieGuest = array(
            'name'  => 'products',
            'value' => 'testval',
            'path'  => $this->generateUrl('frontend_catalog'),
            'time'  => time() + 3600 * 24 * 7
        );

        $cookie = new Cookie($cookieGuest['name'], $cookieGuest['value'], $cookieGuest['time'], $cookieGuest['path']);

        $response = new Response();
        $response->headers->setCookie($cookie);
        $response->send();

        return $this->redirectToRoute('frontend_index');
    }

    /**
     * @Route("/basket", name="frontend_basket")
     * @Template()
     */
    public function basketAction() {
        $items = $this->getProducts();

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $result = array();
        if($items) {
            $qb = $em->createQueryBuilder();
            $qb->select('p')
                ->from('AppCoreBundle:Product', 'p')
                ->where($qb->expr()->in('p.id', array_keys($items))
                )
            ;

            $result = $qb->getQuery()->getResult();
        }


        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();

        $summPrice = 0;

        foreach($result as $product) {
            foreach($items as $id => $count) {
                if($product->getId() == $id) {
                    $summPrice+=$count*$product->getPrice();
                }
            }
        }

        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'slides' => $slides,
            'products' => $result,
            'items' => $items,
            'summPrice' => $summPrice
        );
    }

    /**
     * @Route("/product/add/{id}", name="frontend_product_add")
     */
    public function productAddAction($id, $count = 1) {
        $basket = $this->getProducts();

        $products = $basket;
        $products[$id] = (isset($basket[$id]) && $basket[$id]) ? $basket[$id] + $count : $count;
        $jsonProducts = json_encode($products);

        $cookie = new Cookie('basket', $jsonProducts);

        $response = new Response();

        $response->headers->clearCookie('basket');
        $response->headers->setCookie($cookie);
        $request = new Request();
        $response->prepare($request);
        $response->sendHeaders();


        return $this->redirectToRoute('frontend_catalog');
    }

    /**
     * @Route("/basket/clear", name="frontend_basket_clear")
     */
    public function clearBasketAction() {
        $this->clearBasket();

        return $this->redirectToRoute('frontend_basket');

    }

    /**
     * @Route("/order", name="frontend_order")
     * @Template()
     */
    public function orderAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $order = new ProductOrder();

        $form = $this->createForm(new ProductOrderType(), $order);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $items = $this->getProducts();

            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('p')
                ->from('AppCoreBundle:Product', 'p')
                ->where($qb->expr()->in('p.id', array_keys($items))
                )
            ;

            $result = $qb->getQuery()->getResult();

            $summPrice = 0;

            foreach($result as $product) {
                foreach($items as $id => $count) {
                    if($product->getId() == $id) {
                        $summPrice+=$count*$product->getPrice();
                    }
                }
            }

            $twig = new \Twig_Environment(new \Twig_Loader_String());
            $rendered = $twig->render('<table class="table table-responsive table-bordered">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>Название</td>
                                <td>Количество</td>
                                <td>Цена/шт</td>
                                <td>Цена</td>
                            </tr>
                        </thead>
                        <tbody>
                            {% for product in products%}
                                {% for id, count in items %}
                                    {% if id == product.id %}
                                    <tr>
                                        <td>{{ product.id }}</td>
                                        <td>{{ product.name }}</td>
                                        <td>{{ count }}</td>
                                        <td>{{ product.price }}</td>
                                        <td>{{ product.price*count }}</td>
                                    </tr>
                                    {% endif %}
                                {% endfor %}
                            {% endfor %}
                            <tr>
                                <td><span style="color:red">Итого</span> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><span style="color:red">{{ summPrice }}</span></td>
                            </tr>
                        </tbody>
                    </table>',
                array(
                'products' => $result,
                'items' => $items,
                'summPrice' => $summPrice
                )
            );


            $order->setDone(false);
            $order->setDate(new \DateTime());
            $order->setContent($rendered);

            $em->persist($order);
            $em->flush();



            return $this->redirectToRoute('frontend_basket');
        }

        $slides = $em->getRepository('AppCoreBundle:Slide')->findAll();
        $articleCategory= $em->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        return array(
            'articleCategory'=> $articleCategory,
            'slides' => $slides,
            'form' => $form->createView()
        );
    }

    private function getProducts() {
        $request = $this->get('request');
        $cookies = $request->cookies;

        $products = array();

        if ($cookies->has('basket'))
        {
            $products = json_decode($cookies->get('basket'));
            if($products) {
                $products = get_object_vars($products);
            }
        }

        return $products;
    }

    private function clearBasket() {
        $response = new Response();

        $response->headers->clearCookie('basket');
        $response->sendHeaders();
    }
}
