<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ArticleCategoryType;
use App\CoreBundle\Entity\ArticleCategory;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ArticleCategoryController extends Controller
{
    /**
     * @Route("/articles-categories", name="backend_article_categories")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:ArticleCategory')->findAll();

        $entity = new ArticleCategory();

        $form = $this->createForm(new ArticleCategoryType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_article_categories');
        }

        return array(
            'data' => $data,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/articles-category/delete/{id}", name="backend_article_category_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var ArticleCategory $entity */
        $entity = $em->getRepository('AppCoreBundle:ArticleCategory')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_article_categories');
    }
}
