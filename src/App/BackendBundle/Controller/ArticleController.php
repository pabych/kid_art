<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ArticleType;
use App\CoreBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    /**
     * @Route("/articles", name="backend_articles")
     * @Template()
     */
    public function indexAction()
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:Article')->findAll();

        return array('data' => $data);
    }

    /**
     * @Route("/articles/create", name="backend_article_create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $entity = new Article();

        $form = $this->createForm(new ArticleType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
//            var_dump($form->getData());exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_articles');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/articles/edit/{id}", name="backend_article_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:Article')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $form = $this->createForm(new ArticleType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_articles');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/articles/delete/{id}", name="backend_article_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Article $entity */
        $entity = $em->getRepository('AppCoreBundle:Article')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_articles');
    }
}
