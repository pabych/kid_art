<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ArticleType;
use App\BackendBundle\Form\SlideType;
use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\ProductOrder;
use App\CoreBundle\Entity\Slide;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * @Route("/orders", name="backend_orders")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:ProductOrder')->findAll();

        return array(
            'data' => $data,
        );
    }

    /**
     * @Route("/order/{id}", name="backend_order_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:ProductOrder')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        return array(
            'data' => $entity
        );

    }

    /**
     * @Route("/order/delete/{id}", name="backend_order_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:ProductOrder')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_orders');
    }

    /**
     * @Route("/order/change-status/{id}", name="backend_order_change_status")
     */
    public function changeStatusAction($id) {
        $em = $this->getDoctrine()->getManager();

        /** @var ProductOrder $entity */
        $entity = $em->getRepository('AppCoreBundle:ProductOrder')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

//        var_dump($entity->getPaid());exit;
        if($entity->getDone()) {
            $entity->setDone(false);
        } else {
            $entity->setDone(true);
        }

        $em->persist($entity);

        $em->flush();

        return $this->redirectToRoute('backend_orders');
    }
}
