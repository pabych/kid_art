<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ArticleType;
use App\BackendBundle\Form\SlideType;
use App\CoreBundle\Entity\Article;
use App\CoreBundle\Entity\Slide;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class SlideController extends Controller
{
    /**
     * @Route("/slides", name="backend_slides")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:Slide')->findAll();

        $entity = new Slide();

        $form = $this->createForm(new SlideType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_slides');
        }

        return array(
            'data' => $data,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/slide/delete/{id}", name="backend_slide_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:Slide')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_slides');
    }
}
