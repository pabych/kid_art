<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ArticleCategoryType;
use App\BackendBundle\Form\ProductCategoryType;
use App\CoreBundle\Entity\ArticleCategory;
use App\CoreBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ProductCategoryController extends Controller
{
    /**
     * @Route("/product-categories", name="backend_product_categories")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:ProductCategory')->findAll();

        $entity = new ProductCategory();

        $form = $this->createForm(new ProductCategoryType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_product_categories');
        }

        return array(
            'data' => $data,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/product-category/delete/{id}", name="backend_product_category_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:ProductCategory')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_product_categories');
    }
}
