<?php

namespace App\BackendBundle\Controller;

use App\BackendBundle\Form\ProductType;
use App\CoreBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="backend_products")
     * @Template()
     */
    public function indexAction()
    {
        $data = $this->getDoctrine()->getManager()->getRepository('AppCoreBundle:Product')->findAll();

        return array('data' => $data);
    }

    /**
     * @Route("/products/create", name="backend_product_create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $entity = new Product();

        $form = $this->createForm(new ProductType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
//            var_dump($form->getData());exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_products');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/products/edit/{id}", name="backend_product_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Product $entity */
        $entity = $em->getRepository('AppCoreBundle:Product')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $form = $this->createForm(new ProductType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('backend_products');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/products/delete/{id}", name="backend_product_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppCoreBundle:Product')->find($id);

        if(!$entity) {
            throw new Exception('Entity does not exist');
        }

        $em->remove($entity);

        $em->flush();

        return $this->redirectToRoute('backend_products');
    }
}
