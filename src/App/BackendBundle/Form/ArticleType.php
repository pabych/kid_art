<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => 'Название',
                'attr' => array('class' => 'form-control')
            ))
            ->add('description', null, array(
                'label' => 'Описание',
                'attr' => array('class' => 'form-control')
            ))
            ->add('body', null, array(
                'label' => 'Текст',
                'attr' => array('class' => 'form-control')
            ))
            ->add('categories', 'entity', array(
                'label' => 'Категории',
                'class' => 'AppCoreBundle:ArticleCategory',
                'property' => 'name',
                'required' => false,
                'by_reference' => true,
                'multiple' => true,
                'attr' => array('class' => 'form-control')
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CoreBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'article';
    }
}
