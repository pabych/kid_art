<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'Название',
                'attr' => array('class' => 'form-control')
            ))
            ->add('manufacturer', null, array(
                'label' => 'Производитель',
                'attr' => array('class' => 'form-control')
            ))
            ->add('price', null, array(
                'label' => 'Цена',
                'attr' => array('class' => 'form-control')
            ))
            ->add('description', null, array(
                'label' => 'Описание',
                'attr' => array('class' => 'form-control')
            ))
            ->add('categories', 'entity', array(
                'label' => 'Категории',
                'class' => 'AppCoreBundle:ProductCategory',
                'required' => false,
                'property' => 'name',
                'by_reference' => true,
                'multiple' => true,
                'attr' => array('class' => 'form-control')
            ))

            ->add('isRecommended', 'checkbox', array(
                'label'    => 'Рекомендованные',
                'required' => false
            ))

            ->add('imageFile', 'vich_file', array(
                'label' => false,
                'required'      => false,
                'allow_delete'  => false,
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CoreBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'product';
    }
}
